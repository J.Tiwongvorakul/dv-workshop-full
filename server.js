'use strict'

const path = require('path')

// init config
const nconf = require('nconf')
nconf
  .argv()
  .env('__')
  .defaults({'NODE_ENV': 'development'})

const NODE_ENV = nconf.get('NODE_ENV')
const isDev = NODE_ENV === 'development'

if (!isDev) {
  const fs = require('fs')
  if (!fs.existsSync(path.join(__dirname, `${NODE_ENV}.config.json`))) {
    throw Error(`${path.join(__dirname, `${NODE_ENV}.config.json`)} is not found`)
  }
}

nconf
  .defaults({'conf': path.join(__dirname, `${NODE_ENV}.config.json`)})
  .file(nconf.get('conf'))

// init express
const express = require('express')
const pkg = require('./package.json')
const morgan = require('morgan')

const app = express()

app.use(morgan('dev'))
app.get('/api/version', (_, res) => res.status(200).json(pkg.version))

if (isDev) {
  const webpack = require('webpack')
  const webpackMiddleware = require('webpack-dev-middleware')
  const webpackConfig = require('./webpack.config')
  app.use(webpackMiddleware(webpack(webpackConfig), {
    publicPath: '/',
    stats: {color: true}
  }))
} else {
  app.use(express.static('dist'))
}

app.listen(nconf.get('port'), () => console.log(`Ready on port ${nconf.get('port')}`))